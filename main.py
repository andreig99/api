import pygame
import sys
import numpy as np
import time

pygame.init()
clock = pygame.time.Clock()

screen = pygame.display.set_mode((800, 800))

n = 50

v1 = np.linspace(0, 800, n + 6) # n + 5 deltas
width = v1[1] - v1[0]
space = (5 * width) / (n + 1)   # divide last delta to the no of el + 1 to get spacing including before first el and after last el
v2 = [space]
for i in range(1, n):
    v2.append(v2[i - 1] + width + space)


v3 = np.linspace(0, 800, 251) # 250 deltas
delta = v3[1] - v3[0]
values = np.random.randint(251, size = (n))
height = []
for i in range(n):
    height.append(values[i] * delta)

res = []
res.append(np.array(height))


#ALGORITHM
for i in range(n):
       for j in range(0, n - i - 1):
           if height[j] > height[j + 1]:
               height[j], height[j + 1] = height[j + 1], height[j]
           res.append(np.array(height))




positions_mat = []
dimentions_mat = []
for i in range(len(res)):
    positions = []
    dimentions = []
    for j in range(n):
        positions.append((v2[j], np.ceil(800 - res[i][j])))
        dimentions.append([width, res[i][j]])
    positions_mat.append(positions)
    dimentions_mat.append(dimentions)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    for i in range(len(dimentions_mat)):
        screen.fill((255, 255, 255))
        for j in range(n):
            screen.blit(pygame.Surface(dimentions_mat[i][j]), positions_mat[i][j])
        pygame.display.flip()
        time.sleep(0.01)

    time.sleep(10)
    clock.tick(60)
    pygame.quit()
    sys.exit()